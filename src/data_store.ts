/*
import { DataStore } from "aws-amplify"
import { Result, Ok, Err, Option, Some, None } from "ts-results"
import { PersistentModel, PersistentModelConstructor, ProducerModelPredicate, MutableModel } from "@aws-amplify/datastore"
import { AmplifyError, AmplifyErrorValue } from "./amplify_error"
import { EnumError } from "ts-enum-errors";

const getAuthErrorValueFromString = (value : string ) => value as AmplifyErrorValue;

export async function query<T extends PersistentModel>(modelConstructor: PersistentModelConstructor<T>, id: string) : Promise<Result<Option<T>, AmplifyError>>
{
    try
    {
        //query AppSync
        const result = await DataStore.query(modelConstructor, id);
        //check if there were no results
        return result === undefined ? Ok(None) : Ok(Some(result));
    }
    catch(e)
    {
        return Err(new EnumError<AmplifyErrorValue>(e, AmplifyErrorValue.DatastoreError));
    }
}

//TODO: Implement a safe saveObject API
export async function save<T extends Readonly<{id: string;} & Record<string, any>>>(model: T, condition?: ProducerModelPredicate<T>) : Promise<Result<T, AmplifyError>>
{
    try
    {
        //query AppSync
        const result : T = await DataStore.save(model,condition);
        //return the result of Type T
        return Ok(result);
    }
    catch(e)
    {
        return Err(new EnumError<AmplifyErrorValue>(e, AmplifyErrorValue.DatastoreError));
    }
}

export async function update<T extends Readonly<{id: string;} & Record<string, any>> & PersistentModel>(model: PersistentModelConstructor<T>, id: string, edit_callback : (draft: MutableModel<T>) => void, condition?: ProducerModelPredicate<T>, ) : Promise<Result<T, AmplifyError>>
{
    //safely get the original
    const original = await query(model, id);
    //if there's a DataStoreError getting the original pass it up
    if(original.err) return Err(original.val);
    if(original.val.none) return Err(new EnumError<AmplifyErrorValue>(new Error(), AmplifyErrorValue.DataStoreObjectNotFound));

    try
    {
        //query AppSync
        const result = await DataStore.save(model.copyOf(original.val.val, edit_callback), condition);
        //return the result of Type T
        return Ok(result);
    }
    catch(e)
    {
        return Err(new EnumError<AmplifyErrorValue>(e, AmplifyErrorValue.DatastoreError));
    }
}*/