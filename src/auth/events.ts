//AWS Auth Events (Verbs) we recieve from the Hub
export enum AWSAuthEvent 
{
    signIn = 'signIn',
    signUp = 'signUp',
    signOut = 'signOut',
    signIn_failure = 'signIn_failure',
    configured = 'configured'
}

