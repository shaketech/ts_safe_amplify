import { Auth } from "aws-amplify";
import { CognitoUser } from "@aws-amplify/auth";
import { AuthError } from "./error";
import { CognitoUserSession, ISignUpResult, CognitoUserAttribute } from "amazon-cognito-identity-js";
import { toAuthResult } from "./error";
import { ClientMetaData, SignUpParams, CurrentUserOpts, FederatedSignInOptions, GetPreferredMFAOpts, SignInOpts } from "@aws-amplify/auth/src/types";
import { ICredentials } from "@aws-amplify/core";
import { Result } from "ts-results";

export type { SignUpParams }

export * as error from "./error";

//https://aws-amplify.github.io/amplify-js/api/classes/authclass.html
export { 
    change_password,                    federated_sign_in,                  sign_up,                                    verify_user_attribute_submit,
    complete_new_password,              forgot_password,                    update_user_attributes,                     setup_totp,
    confirm_sign_in,                    forgot_password_submit,             user_attributes,
    confirm_sign_up,                    get_preferred_mfa,                  user_session,
    current_authenticated_user,         resend_sign_up,                     verified_contact,
    current_credentials,                send_custom_challenge_answer,       verify_current_user_attribute,
    current_session,                    set_preferred_mfa,                  verify_current_user_attribute_submit,
    current_user_credentials,           sign_in,                            verify_totp_token,
    current_user_pool_user,             sign_out,                           verify_user_attribute,
};


async function change_password(user : CognitoUser, old_password : string, new_password : string) : Promise<Result<string,AuthError>>
{
    return toAuthResult<string>(Auth.changePassword(user, old_password, new_password));
}

//Require New Password 
//Used for creating accounts in the AWS console instead of through the frontend
async function complete_new_password(user : CognitoUser, new_password : string, required_parameters : any) : Promise<Result<void , AuthError>>
{
    return toAuthResult<void>(Auth.completeNewPassword(user, new_password, required_parameters));
}

//Confirm Sign In
async function confirm_sign_in(user : CognitoUser, code : string, mfaType?: "SMS_MFA" | "SOFTWARE_TOKEN_MFA" | null, client_metadata?: ClientMetaData) : Promise<Result<CognitoUser , AuthError>>
{
    return toAuthResult<CognitoUser>(Auth.confirmSignIn(user, code, mfaType, client_metadata));
}

//Confirm Sign Up
async function confirm_sign_up(username : string, code : string) : Promise<Result<string , AuthError>>
{
    //I checked and it looks like this function either returns an Error or the string 'SUCCESS'
    return toAuthResult<string>(Auth.confirmSignUp(username, code));
} 

//Current Authenticated User
async function current_authenticated_user(params?: CurrentUserOpts) : Promise<Result<CognitoUser, AuthError>>
{
    return toAuthResult<CognitoUser>(Auth.currentAuthenticatedUser(params));
}

//Current Credentials
async function current_credentials() : Promise<Result<ICredentials , AuthError>>
{
    return toAuthResult<ICredentials>(Auth.currentCredentials());
}

//Current Session
async function current_session() : Promise<Result<CognitoUserSession , AuthError>>
{
    return toAuthResult<CognitoUserSession>(Auth.currentSession());
}

async function current_user_credentials() : Promise<Result<ICredentials , AuthError>>
{
    return toAuthResult<ICredentials>(Auth.currentUserCredentials());
}

async function current_user_pool_user(params?: CurrentUserOpts) : Promise<Result<CognitoUser , AuthError>>
{
    return toAuthResult<CognitoUser>(Auth.currentUserPoolUser(params));
}

async function federated_sign_in(options?: FederatedSignInOptions) : Promise<Result<ICredentials , AuthError>>
{
    return toAuthResult<ICredentials>(Auth.federatedSignIn(options));
}

//Forgot Password
async function forgot_password(username : string) : Promise<Result<void , AuthError>>
{
    return toAuthResult<void>(Auth.forgotPassword(username));
}

async function forgot_password_submit(username: string, code: string, password: string, client_metadata?: ClientMetaData) : Promise<Result<string , AuthError>>
{
    return toAuthResult<string>(Auth.forgotPasswordSubmit(username, code, password, client_metadata));
}

async function get_preferred_mfa(user : CognitoUser, params?: GetPreferredMFAOpts) : Promise<Result<string , AuthError>>
{
    return toAuthResult<string>(Auth.getPreferredMFA(user, params));
}

async function resend_sign_up(username: string, client_metadata?: ClientMetaData) : Promise<Result<string , AuthError>>
{
    return toAuthResult<string>(Auth.resendSignUp(username, client_metadata));
}

async function send_custom_challenge_answer(user: CognitoUser, challenge_responses: string, client_metadata?: ClientMetaData) : Promise<Result<CognitoUser , AuthError>>
{
    return toAuthResult<CognitoUser>(Auth.sendCustomChallengeAnswer(user, challenge_responses, client_metadata));
}

//Set Preferred MFA
async function set_preferred_mfa(user : CognitoUser, mfaMethod : "SMS" | "TOTP" | "NOMFA") : Promise<Result<string , AuthError>>
{
    return toAuthResult<string>(Auth.setPreferredMFA(user,  mfaMethod));
}

//TOTP Setup
async function setup_totp(user : CognitoUser) : Promise<Result<string , AuthError>>
{
    return toAuthResult<string>(Auth.setupTOTP(user));
}

//Sign In
async function sign_in(usernameOrSignInOpts: string | SignInOpts, password?: string) : Promise<Result<CognitoUser , AuthError>>
{
    return toAuthResult<CognitoUser>(Auth.signIn(usernameOrSignInOpts, password));
}

//Sign Out
async function sign_out() : Promise<Result<void , AuthError>>
{
    //this function has no return type it only resolves
    return toAuthResult<void>(Auth.signOut());
}

//Sign Up
async function sign_up(params : SignUpParams) : Promise<Result<ISignUpResult , AuthError>>
{
    return toAuthResult<ISignUpResult>(Auth.signUp(params));
}

async function update_user_attributes(user: CognitoUser, attributes: object, client_metadata?: ClientMetaData) : Promise<Result<string , AuthError>>
{
    return toAuthResult<string>(Auth.updateUserAttributes(user, attributes, client_metadata));
}

async function user_attributes(user: CognitoUser) : Promise<Result<CognitoUserAttribute[] , AuthError>>
{
    return toAuthResult<CognitoUserAttribute[]>(Auth.userAttributes(user));
}

async function user_session(user : CognitoUser) : Promise<Result<CognitoUserSession , AuthError>>
{
    return toAuthResult<CognitoUserSession>(Auth.userSession(user));
}

export interface VerifiedContact
{
    verified : 
    {
        email?:string,
        phone_number?: string
    },
    unverified : 
    {
        email?: string,
        phone_number?: string
    }
}

//Verify Contact
async function verified_contact(user : CognitoUser) : Promise<Result<VerifiedContact , AuthError>>
{
    return toAuthResult<VerifiedContact>(Auth.verifiedContact(user));
}

async function verify_current_user_attribute(attr : string) : Promise<Result<void , AuthError>>
{
    return toAuthResult<void>(Auth.verifyCurrentUserAttribute(attr));
}

async function verify_current_user_attribute_submit(attr: string, code: string) : Promise<Result<string , AuthError>>
{
    return toAuthResult<string>(Auth.verifyCurrentUserAttributeSubmit(attr, code));
}

//Verify TOTP Token
async function verify_totp_token(user : CognitoUser, token : string) : Promise<Result<CognitoUserSession , AuthError>>
{
    return toAuthResult<CognitoUserSession>(Auth.verifyTotpToken(user, token));
}

async function verify_user_attribute(user: CognitoUser , attr: string, client_metadata?: ClientMetaData): Promise<Result<void , AuthError>>
{
    return toAuthResult<void>(Auth.verifyUserAttribute(user, attr, client_metadata));
}

async function verify_user_attribute_submit(user: CognitoUser, attr: string, code: string): Promise<Result<string , AuthError>>
{
    return toAuthResult<string>(Auth.verifyUserAttributeSubmit(user, attr, code))
}

//TODO: Force Email Uniqueness
//https://docs.amplify.aws/lib/auth/mfa/q/platform/js#forcing-email-uniqueness-in-cognito-user-pools