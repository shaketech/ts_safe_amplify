import { AuthErrorValue, toAuthResult } from "./error";

test("Parse NoConfig error into AuthErrorValue::NoConfig", async () =>
{
    const safe_error = await toAuthResult<string>(no_config_error());

    if(safe_error.err) 
    {
        const error = safe_error.val;
        expect(error.type === AuthErrorValue.NoConfig);
    } 
});

async function no_config_error() : Promise<string>
{
    throw new Error('noConfig');
}