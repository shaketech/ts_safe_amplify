import { EnumError } from 'ts-enum-errors'
import { Result, Ok, Err, Option, Some, None } from "ts-results";

export type AuthError = EnumError<AuthErrorValue>;

export enum AuthErrorValue
{
    //Defined Auth Error Types
    NoCognitoUserSignedIn = "The user is not authenticated",
    InvalidPhoneFormat = "Invalid phone number format",
    UserNotConfirmedException = "User is not confirmed.",
    NoConfig = "noConfig",
	MissingAuthConfig = "missingAuthConfig",
	EmptyUsername = "emptyUsername",
	InvalidUsername = "invalidUsername",
	EmptyPassword = "emptyPassword",
	EmptyCode = "emptyCode",
	SignUpError = "signUpError",
	NoMFA = "noMFA",
	InvalidMFA = "invalidMFA",
	EmptyChallengeResponse = "emptyChallengeResponse",
	NoUserSession = "noUserSession",
	Default = "default"
}

const getAuthErrorValueFromString = (value : string ) => value as AuthErrorValue;

export async function toAuthResult<T>(promise: Promise<T>) : Promise<Result<T , AuthError>>
{
    return promise
    .then(result => Ok(result))
    .catch(e => Err(new EnumError<AuthErrorValue>(e, getAuthErrorValueFromString)));
}

export async function toNullableAuthResult<T>(promise: Promise<T>) : Promise<Result<Option<T>, AuthError>>
{
    return promise
    .then( value  => value === undefined ? Ok(None) : Ok(Some(value)))
    .catch(e => Err(new EnumError<AuthErrorValue>(e, getAuthErrorValueFromString)));
}