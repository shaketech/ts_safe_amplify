import { EnumError } from 'ts-enum-errors';

export enum AmplifyErrorValue
{
    InvalidModelConstructor = "Constructor is not for a valid model",
    DataStoreObjectNotFound = "The DataStore update operation failed because the original object could not be found in the database!",
    CognitoUserNotFound = "CognitoUserNotFoundError: No CognitoUser currently authenticated!",
    //This type of error is a wrapper for the untyped errors we recieve from Amplify APIs
    //TODO: make a parser that returns an APIErrorValue from the error message so we can more 
    //easily identify errors that aren't typed
    DatastoreError = "DatastoreError",
    AuthError = "AmplifyAuthError",
    CognitoError = "CognitoError",
    CognitoSignUpFailed = "CognitoSignUpFailed"
}

export type AmplifyError = EnumError<AmplifyErrorValue>