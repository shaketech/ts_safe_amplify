//AWS Auth Events (Verbs) we recieve from the Hub
export var AWSAuthEvent;
(function (AWSAuthEvent) {
    AWSAuthEvent["signIn"] = "signIn";
    AWSAuthEvent["signUp"] = "signUp";
    AWSAuthEvent["signOut"] = "signOut";
    AWSAuthEvent["signIn_failure"] = "signIn_failure";
    AWSAuthEvent["configured"] = "configured";
})(AWSAuthEvent || (AWSAuthEvent = {}));
