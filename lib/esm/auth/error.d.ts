import { EnumError } from 'ts-enum-errors';
import { Result, Option } from "ts-results";
export declare type AuthError = EnumError<AuthErrorValue>;
export declare enum AuthErrorValue {
    NoCognitoUserSignedIn = "The user is not authenticated",
    InvalidPhoneFormat = "Invalid phone number format",
    UserNotConfirmedException = "User is not confirmed.",
    NoConfig = "noConfig",
    MissingAuthConfig = "missingAuthConfig",
    EmptyUsername = "emptyUsername",
    InvalidUsername = "invalidUsername",
    EmptyPassword = "emptyPassword",
    EmptyCode = "emptyCode",
    SignUpError = "signUpError",
    NoMFA = "noMFA",
    InvalidMFA = "invalidMFA",
    EmptyChallengeResponse = "emptyChallengeResponse",
    NoUserSession = "noUserSession",
    Default = "default"
}
export declare function toAuthResult<T>(promise: Promise<T>): Promise<Result<T, AuthError>>;
export declare function toNullableAuthResult<T>(promise: Promise<T>): Promise<Result<Option<T>, AuthError>>;
