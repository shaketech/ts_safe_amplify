import { CognitoUser } from "@aws-amplify/auth";
import { AuthError } from "./error";
import { CognitoUserSession, ISignUpResult, CognitoUserAttribute } from "amazon-cognito-identity-js";
import { ClientMetaData, SignUpParams, CurrentUserOpts, FederatedSignInOptions, GetPreferredMFAOpts, SignInOpts } from "@aws-amplify/auth/src/types";
import { ICredentials } from "@aws-amplify/core";
import { Result } from "ts-results";
export type { SignUpParams };
export * as error from "./error";
export { change_password, federated_sign_in, sign_up, verify_user_attribute_submit, complete_new_password, forgot_password, update_user_attributes, setup_totp, confirm_sign_in, forgot_password_submit, user_attributes, confirm_sign_up, get_preferred_mfa, user_session, current_authenticated_user, resend_sign_up, verified_contact, current_credentials, send_custom_challenge_answer, verify_current_user_attribute, current_session, set_preferred_mfa, verify_current_user_attribute_submit, current_user_credentials, sign_in, verify_totp_token, current_user_pool_user, sign_out, verify_user_attribute, };
declare function change_password(user: CognitoUser, old_password: string, new_password: string): Promise<Result<string, AuthError>>;
declare function complete_new_password(user: CognitoUser, new_password: string, required_parameters: any): Promise<Result<void, AuthError>>;
declare function confirm_sign_in(user: CognitoUser, code: string, mfaType?: "SMS_MFA" | "SOFTWARE_TOKEN_MFA" | null, client_metadata?: ClientMetaData): Promise<Result<CognitoUser, AuthError>>;
declare function confirm_sign_up(username: string, code: string): Promise<Result<string, AuthError>>;
declare function current_authenticated_user(params?: CurrentUserOpts): Promise<Result<CognitoUser, AuthError>>;
declare function current_credentials(): Promise<Result<ICredentials, AuthError>>;
declare function current_session(): Promise<Result<CognitoUserSession, AuthError>>;
declare function current_user_credentials(): Promise<Result<ICredentials, AuthError>>;
declare function current_user_pool_user(params?: CurrentUserOpts): Promise<Result<CognitoUser, AuthError>>;
declare function federated_sign_in(options?: FederatedSignInOptions): Promise<Result<ICredentials, AuthError>>;
declare function forgot_password(username: string): Promise<Result<void, AuthError>>;
declare function forgot_password_submit(username: string, code: string, password: string, client_metadata?: ClientMetaData): Promise<Result<string, AuthError>>;
declare function get_preferred_mfa(user: CognitoUser, params?: GetPreferredMFAOpts): Promise<Result<string, AuthError>>;
declare function resend_sign_up(username: string, client_metadata?: ClientMetaData): Promise<Result<string, AuthError>>;
declare function send_custom_challenge_answer(user: CognitoUser, challenge_responses: string, client_metadata?: ClientMetaData): Promise<Result<CognitoUser, AuthError>>;
declare function set_preferred_mfa(user: CognitoUser, mfaMethod: "SMS" | "TOTP" | "NOMFA"): Promise<Result<string, AuthError>>;
declare function setup_totp(user: CognitoUser): Promise<Result<string, AuthError>>;
declare function sign_in(usernameOrSignInOpts: string | SignInOpts, password?: string): Promise<Result<CognitoUser, AuthError>>;
declare function sign_out(): Promise<Result<void, AuthError>>;
declare function sign_up(params: SignUpParams): Promise<Result<ISignUpResult, AuthError>>;
declare function update_user_attributes(user: CognitoUser, attributes: object, client_metadata?: ClientMetaData): Promise<Result<string, AuthError>>;
declare function user_attributes(user: CognitoUser): Promise<Result<CognitoUserAttribute[], AuthError>>;
declare function user_session(user: CognitoUser): Promise<Result<CognitoUserSession, AuthError>>;
export interface VerifiedContact {
    verified: {
        email?: string;
        phone_number?: string;
    };
    unverified: {
        email?: string;
        phone_number?: string;
    };
}
declare function verified_contact(user: CognitoUser): Promise<Result<VerifiedContact, AuthError>>;
declare function verify_current_user_attribute(attr: string): Promise<Result<void, AuthError>>;
declare function verify_current_user_attribute_submit(attr: string, code: string): Promise<Result<string, AuthError>>;
declare function verify_totp_token(user: CognitoUser, token: string): Promise<Result<CognitoUserSession, AuthError>>;
declare function verify_user_attribute(user: CognitoUser, attr: string, client_metadata?: ClientMetaData): Promise<Result<void, AuthError>>;
declare function verify_user_attribute_submit(user: CognitoUser, attr: string, code: string): Promise<Result<string, AuthError>>;
