var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Auth } from "aws-amplify";
import { toAuthResult } from "./error";
export * as error from "./error";
//https://aws-amplify.github.io/amplify-js/api/classes/authclass.html
export { change_password, federated_sign_in, sign_up, verify_user_attribute_submit, complete_new_password, forgot_password, update_user_attributes, setup_totp, confirm_sign_in, forgot_password_submit, user_attributes, confirm_sign_up, get_preferred_mfa, user_session, current_authenticated_user, resend_sign_up, verified_contact, current_credentials, send_custom_challenge_answer, verify_current_user_attribute, current_session, set_preferred_mfa, verify_current_user_attribute_submit, current_user_credentials, sign_in, verify_totp_token, current_user_pool_user, sign_out, verify_user_attribute, };
function change_password(user, old_password, new_password) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.changePassword(user, old_password, new_password))];
        });
    });
}
//Require New Password 
//Used for creating accounts in the AWS console instead of through the frontend
function complete_new_password(user, new_password, required_parameters) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.completeNewPassword(user, new_password, required_parameters))];
        });
    });
}
//Confirm Sign In
function confirm_sign_in(user, code, mfaType, client_metadata) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.confirmSignIn(user, code, mfaType, client_metadata))];
        });
    });
}
//Confirm Sign Up
function confirm_sign_up(username, code) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            //I checked and it looks like this function either returns an Error or the string 'SUCCESS'
            return [2 /*return*/, toAuthResult(Auth.confirmSignUp(username, code))];
        });
    });
}
//Current Authenticated User
function current_authenticated_user(params) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.currentAuthenticatedUser(params))];
        });
    });
}
//Current Credentials
function current_credentials() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.currentCredentials())];
        });
    });
}
//Current Session
function current_session() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.currentSession())];
        });
    });
}
function current_user_credentials() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.currentUserCredentials())];
        });
    });
}
function current_user_pool_user(params) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.currentUserPoolUser(params))];
        });
    });
}
function federated_sign_in(options) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.federatedSignIn(options))];
        });
    });
}
//Forgot Password
function forgot_password(username) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.forgotPassword(username))];
        });
    });
}
function forgot_password_submit(username, code, password, client_metadata) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.forgotPasswordSubmit(username, code, password, client_metadata))];
        });
    });
}
function get_preferred_mfa(user, params) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.getPreferredMFA(user, params))];
        });
    });
}
function resend_sign_up(username, client_metadata) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.resendSignUp(username, client_metadata))];
        });
    });
}
function send_custom_challenge_answer(user, challenge_responses, client_metadata) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.sendCustomChallengeAnswer(user, challenge_responses, client_metadata))];
        });
    });
}
//Set Preferred MFA
function set_preferred_mfa(user, mfaMethod) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.setPreferredMFA(user, mfaMethod))];
        });
    });
}
//TOTP Setup
function setup_totp(user) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.setupTOTP(user))];
        });
    });
}
//Sign In
function sign_in(usernameOrSignInOpts, password) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.signIn(usernameOrSignInOpts, password))];
        });
    });
}
//Sign Out
function sign_out() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            //this function has no return type it only resolves
            return [2 /*return*/, toAuthResult(Auth.signOut())];
        });
    });
}
//Sign Up
function sign_up(params) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.signUp(params))];
        });
    });
}
function update_user_attributes(user, attributes, client_metadata) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.updateUserAttributes(user, attributes, client_metadata))];
        });
    });
}
function user_attributes(user) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.userAttributes(user))];
        });
    });
}
function user_session(user) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.userSession(user))];
        });
    });
}
//Verify Contact
function verified_contact(user) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.verifiedContact(user))];
        });
    });
}
function verify_current_user_attribute(attr) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.verifyCurrentUserAttribute(attr))];
        });
    });
}
function verify_current_user_attribute_submit(attr, code) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.verifyCurrentUserAttributeSubmit(attr, code))];
        });
    });
}
//Verify TOTP Token
function verify_totp_token(user, token) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.verifyTotpToken(user, token))];
        });
    });
}
function verify_user_attribute(user, attr, client_metadata) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.verifyUserAttribute(user, attr, client_metadata))];
        });
    });
}
function verify_user_attribute_submit(user, attr, code) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, toAuthResult(Auth.verifyUserAttributeSubmit(user, attr, code))];
        });
    });
}
//TODO: Force Email Uniqueness
//https://docs.amplify.aws/lib/auth/mfa/q/platform/js#forcing-email-uniqueness-in-cognito-user-pools
