export declare enum AWSAuthEvent {
    signIn = "signIn",
    signUp = "signUp",
    signOut = "signOut",
    signIn_failure = "signIn_failure",
    configured = "configured"
}
