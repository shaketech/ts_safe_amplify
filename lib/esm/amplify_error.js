export var AmplifyErrorValue;
(function (AmplifyErrorValue) {
    AmplifyErrorValue["InvalidModelConstructor"] = "Constructor is not for a valid model";
    AmplifyErrorValue["DataStoreObjectNotFound"] = "The DataStore update operation failed because the original object could not be found in the database!";
    AmplifyErrorValue["CognitoUserNotFound"] = "CognitoUserNotFoundError: No CognitoUser currently authenticated!";
    //This type of error is a wrapper for the untyped errors we recieve from Amplify APIs
    //TODO: make a parser that returns an APIErrorValue from the error message so we can more 
    //easily identify errors that aren't typed
    AmplifyErrorValue["DatastoreError"] = "DatastoreError";
    AmplifyErrorValue["AuthError"] = "AmplifyAuthError";
    AmplifyErrorValue["CognitoError"] = "CognitoError";
    AmplifyErrorValue["CognitoSignUpFailed"] = "CognitoSignUpFailed";
})(AmplifyErrorValue || (AmplifyErrorValue = {}));
