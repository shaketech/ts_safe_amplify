"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AWSAuthEvent = void 0;
//AWS Auth Events (Verbs) we recieve from the Hub
var AWSAuthEvent;
(function (AWSAuthEvent) {
    AWSAuthEvent["signIn"] = "signIn";
    AWSAuthEvent["signUp"] = "signUp";
    AWSAuthEvent["signOut"] = "signOut";
    AWSAuthEvent["signIn_failure"] = "signIn_failure";
    AWSAuthEvent["configured"] = "configured";
})(AWSAuthEvent = exports.AWSAuthEvent || (exports.AWSAuthEvent = {}));
