"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.verify_user_attribute = exports.sign_out = exports.current_user_pool_user = exports.verify_totp_token = exports.sign_in = exports.current_user_credentials = exports.verify_current_user_attribute_submit = exports.set_preferred_mfa = exports.current_session = exports.verify_current_user_attribute = exports.send_custom_challenge_answer = exports.current_credentials = exports.verified_contact = exports.resend_sign_up = exports.current_authenticated_user = exports.user_session = exports.get_preferred_mfa = exports.confirm_sign_up = exports.user_attributes = exports.forgot_password_submit = exports.confirm_sign_in = exports.setup_totp = exports.update_user_attributes = exports.forgot_password = exports.complete_new_password = exports.verify_user_attribute_submit = exports.sign_up = exports.federated_sign_in = exports.change_password = exports.error = void 0;
var aws_amplify_1 = require("aws-amplify");
var error_1 = require("./error");
exports.error = require("./error");
function change_password(user, old_password, new_password) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.changePassword(user, old_password, new_password))];
        });
    });
}
exports.change_password = change_password;
//Require New Password 
//Used for creating accounts in the AWS console instead of through the frontend
function complete_new_password(user, new_password, required_parameters) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.completeNewPassword(user, new_password, required_parameters))];
        });
    });
}
exports.complete_new_password = complete_new_password;
//Confirm Sign In
function confirm_sign_in(user, code, mfaType, client_metadata) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.confirmSignIn(user, code, mfaType, client_metadata))];
        });
    });
}
exports.confirm_sign_in = confirm_sign_in;
//Confirm Sign Up
function confirm_sign_up(username, code) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            //I checked and it looks like this function either returns an Error or the string 'SUCCESS'
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.confirmSignUp(username, code))];
        });
    });
}
exports.confirm_sign_up = confirm_sign_up;
//Current Authenticated User
function current_authenticated_user(params) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.currentAuthenticatedUser(params))];
        });
    });
}
exports.current_authenticated_user = current_authenticated_user;
//Current Credentials
function current_credentials() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.currentCredentials())];
        });
    });
}
exports.current_credentials = current_credentials;
//Current Session
function current_session() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.currentSession())];
        });
    });
}
exports.current_session = current_session;
function current_user_credentials() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.currentUserCredentials())];
        });
    });
}
exports.current_user_credentials = current_user_credentials;
function current_user_pool_user(params) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.currentUserPoolUser(params))];
        });
    });
}
exports.current_user_pool_user = current_user_pool_user;
function federated_sign_in(options) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.federatedSignIn(options))];
        });
    });
}
exports.federated_sign_in = federated_sign_in;
//Forgot Password
function forgot_password(username) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.forgotPassword(username))];
        });
    });
}
exports.forgot_password = forgot_password;
function forgot_password_submit(username, code, password, client_metadata) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.forgotPasswordSubmit(username, code, password, client_metadata))];
        });
    });
}
exports.forgot_password_submit = forgot_password_submit;
function get_preferred_mfa(user, params) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.getPreferredMFA(user, params))];
        });
    });
}
exports.get_preferred_mfa = get_preferred_mfa;
function resend_sign_up(username, client_metadata) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.resendSignUp(username, client_metadata))];
        });
    });
}
exports.resend_sign_up = resend_sign_up;
function send_custom_challenge_answer(user, challenge_responses, client_metadata) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.sendCustomChallengeAnswer(user, challenge_responses, client_metadata))];
        });
    });
}
exports.send_custom_challenge_answer = send_custom_challenge_answer;
//Set Preferred MFA
function set_preferred_mfa(user, mfaMethod) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.setPreferredMFA(user, mfaMethod))];
        });
    });
}
exports.set_preferred_mfa = set_preferred_mfa;
//TOTP Setup
function setup_totp(user) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.setupTOTP(user))];
        });
    });
}
exports.setup_totp = setup_totp;
//Sign In
function sign_in(usernameOrSignInOpts, password) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.signIn(usernameOrSignInOpts, password))];
        });
    });
}
exports.sign_in = sign_in;
//Sign Out
function sign_out() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            //this function has no return type it only resolves
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.signOut())];
        });
    });
}
exports.sign_out = sign_out;
//Sign Up
function sign_up(params) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.signUp(params))];
        });
    });
}
exports.sign_up = sign_up;
function update_user_attributes(user, attributes, client_metadata) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.updateUserAttributes(user, attributes, client_metadata))];
        });
    });
}
exports.update_user_attributes = update_user_attributes;
function user_attributes(user) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.userAttributes(user))];
        });
    });
}
exports.user_attributes = user_attributes;
function user_session(user) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.userSession(user))];
        });
    });
}
exports.user_session = user_session;
//Verify Contact
function verified_contact(user) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.verifiedContact(user))];
        });
    });
}
exports.verified_contact = verified_contact;
function verify_current_user_attribute(attr) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.verifyCurrentUserAttribute(attr))];
        });
    });
}
exports.verify_current_user_attribute = verify_current_user_attribute;
function verify_current_user_attribute_submit(attr, code) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.verifyCurrentUserAttributeSubmit(attr, code))];
        });
    });
}
exports.verify_current_user_attribute_submit = verify_current_user_attribute_submit;
//Verify TOTP Token
function verify_totp_token(user, token) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.verifyTotpToken(user, token))];
        });
    });
}
exports.verify_totp_token = verify_totp_token;
function verify_user_attribute(user, attr, client_metadata) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.verifyUserAttribute(user, attr, client_metadata))];
        });
    });
}
exports.verify_user_attribute = verify_user_attribute;
function verify_user_attribute_submit(user, attr, code) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, (0, error_1.toAuthResult)(aws_amplify_1.Auth.verifyUserAttributeSubmit(user, attr, code))];
        });
    });
}
exports.verify_user_attribute_submit = verify_user_attribute_submit;
//TODO: Force Email Uniqueness
//https://docs.amplify.aws/lib/auth/mfa/q/platform/js#forcing-email-uniqueness-in-cognito-user-pools
