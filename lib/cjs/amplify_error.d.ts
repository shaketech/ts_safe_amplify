import { EnumError } from 'ts-enum-errors';
export declare enum AmplifyErrorValue {
    InvalidModelConstructor = "Constructor is not for a valid model",
    DataStoreObjectNotFound = "The DataStore update operation failed because the original object could not be found in the database!",
    CognitoUserNotFound = "CognitoUserNotFoundError: No CognitoUser currently authenticated!",
    DatastoreError = "DatastoreError",
    AuthError = "AmplifyAuthError",
    CognitoError = "CognitoError",
    CognitoSignUpFailed = "CognitoSignUpFailed"
}
export declare type AmplifyError = EnumError<AmplifyErrorValue>;
